﻿using UnityEngine;

public class Chunk {
    MeshFilter mfilter;
    MeshRenderer mrenderer;

    MeshCollider mcollider;
    GameObject self;

    Vector3 origin;
    Density density;
    MeshData meshData;
    RandomObject[] trees;

    public void Activate(){
        self.SetActive(true);
    }

    public void Deactivate(){
        self.SetActive(false);
    }

    public float this[Vector3 key]{
        get => density[Index(key)];
        set => density[Index(key)] = value;
    }

    Vector3Int Index(Vector3 key){
        var res = key - origin;
        return VectorExt.ToVector3Int(res);
    }

    public void Remove(Vector3 pos, float strength){
        /* we decrease the density to remove a block */
        this[pos] = this[pos] - strength;
    }

    public void Place(Vector3 pos, float strength){
        /* We increase the density to place a block */
        this[pos] = this[pos] + strength;
    }

    public bool Contains(Vector3 pos){
        /* The inner density is always indexed from (0,0,0).
        Therefore we have to subtract our current origin */
        return density.Contains(Index(pos));
    }


    /* Chunk creates and stores the density function
    creation is done using PerlinNoise heightmap.
    If above groundlevel density is chosen such that it is outside.
    If below groundlevel density is chosen to be inside the object.
    After the initial creation the density function can be changed by
    adding & removing blocks.
    After creation or after updating the density function, the chunk
    can be turned into a mesh which is then visible inside unity. */
    public Chunk(Vector3 origin, Transform transform, Material material, RandomObject[] trees){
        self = new GameObject();//interface connection
        self.transform.SetParent(transform);//hierarchy
        mfilter = self.AddComponent<MeshFilter>();
        mrenderer = self.AddComponent<MeshRenderer>();
        mcollider = self.AddComponent<MeshCollider>();
        mrenderer.material = material;
        this.origin = origin;
        this.trees = trees;
    }

    public bool Empty(){
        return density.Empty();
    }

    public void Generate(){
        /* although there are only 20x20x20 blocks inside a Chunk,
        we have to have the values for the 21x21x21 corners of the blocks.
        That means that the chunks share the corners at the outer edge
        with their neighbours */
        var dens = Generate_GPU();
        density = new Density(ChunkPosition.size + Vector3Int.one, dens);
    }

    float[] Generate_GPU(){
        return FastDensity.instance.Density(origin);
    }

    float[] Generate_CPU(){
        return SlowDensity.Density(origin);
    }

    /* Render the current density function into a mesh */
    public void MarchingCubes(){
        meshData = MarchingCubes_GPU();
    }

    MeshData MarchingCubes_GPU(){
        return FastMesh.instance.MarchingCubes(density.densities, origin);
    }
    MeshData MarchingCubes_CPU(){
        return SlowMesh.MarchingCubes(density.densities, origin);
    }

    public void Render(){
        meshData.normals = SmoothMesh.SmoothNormals(meshData);
        var mesh = meshData.toMesh();
        mfilter.sharedMesh = mesh;
        mcollider.sharedMesh = mesh; //asign mesh to meshfilter
    }

    public void PlaceTrees(){
        Random.State originalRandomState = Random.state;
        Random.InitState(origin.GetHashCode());
        var size = new Vector2Int(ChunkPosition.size.x, ChunkPosition.size.z);
        foreach(var tree in this.trees){
            var toPlace = Poisson.Sample(size, tree.distance);
            foreach(var v in toPlace){
                var top = new Vector3(v.x, ChunkPosition.size.y, v.y) + origin;
                RaycastHit hit;
                if(Physics.Raycast(top, Vector3.down, out hit, Mathf.Infinity) && Contains(hit.point)){
                    var rotation = Quaternion.Euler(Random.Range(0, tree.rotation.x), Random.Range(0, tree.rotation.y), Random.Range(0, tree.rotation.z));
                    var selected = tree.objs[Random.Range(0, tree.objs.Length)];
                    var obj = GameObject.Instantiate(selected, hit.point, rotation, self.transform);
                    obj.transform.localScale = Vector3.one * tree.scale;
                }
            }
        }
        Random.state = originalRandomState;
    }
}
