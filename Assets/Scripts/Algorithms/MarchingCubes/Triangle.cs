using UnityEngine;
using System.Linq;

struct Triangle {
    public Vector3 a, b, c;
    public Vector3[] data(){
        return new Vector3[]{a, b, c};
    }
}

class TriangleMesh {
    public static MeshData ToMeshData(Triangle[] tris){
        var vertices = tris
            .SelectMany(x => x.data())
            .ToArray();
        var triangles = Enumerable.Range(0, vertices.Length)
            .ToArray();
        var uv = ComputeUV(tris);
        return new MeshData {
            vertices = vertices,
            triangles = triangles,
            uv = uv
        };
    }

    public static Vector2[] ComputeUV(Triangle[] tris){
        return tris.SelectMany(x => {
            var normal = Vector3.Cross(x.c - x.a, x.b - x.a); //cross product for calculating normal vector
            var rot = Quaternion.Inverse(Quaternion.LookRotation(normal)); //rotate to get flat surface
            return new Vector2[]{rot * x.a, rot * x.b, rot * x.c};//turn into vector2
        }).ToArray();
    }
}