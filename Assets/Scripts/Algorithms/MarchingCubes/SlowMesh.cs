﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

static class SlowMesh {
    static Vector3Int size = ChunkPosition.size + Vector3Int.one;

    static float isoLevel = 0; //threhold of density to become material

    static Vector3Int[] vertices = {
        new Vector3Int(0, 0, 0),
        new Vector3Int(1, 0, 0),
        new Vector3Int(1, 0, 1),
        new Vector3Int(0, 0, 1),
        new Vector3Int(0, 1, 0),
        new Vector3Int(1, 1, 0),
        new Vector3Int(1, 1, 1),
        new Vector3Int(0, 1, 1),
    };

    static int Index(Vector3Int id){//convension of the 3D index
        return id.x + id.y * size.x + id.z * size.x * size.y;
    }

    static float inter(float a, float b) {
        return (isoLevel - a) / (b - a);
    }


    public static MeshData MarchingCubes(float[] density, Vector3 origin){
        
        var triangles = new List<Triangle>();
        for(var x=0; x<size.x-1; x++){
            for(var y=0; y<size.y-1; y++){
                for(var z=0; z<size.z-1; z++){
                    var id = new Vector3Int(x, y, z);

                    var d = new float[8];
                    for(var i=0; i<8; i++)
                    {
                        d[i] = density[Index(id + vertices[i])];//get all the density value at the cune corners
                    }   

                    var idx = 0;
                    for(var i=0; i<8; i++)
                    {
                        if (d[i] > isoLevel) idx |= (1 << i); //index in triangulation table of 256 corner state
                    }   

                    for(var i=0; MarchTables.triangulation[idx][3*i] != -1; i++) {
                        Vector3[] t = new Vector3[3];
                        for(var j=0; j<3; j++){
                            var edge = MarchTables.triangulation[idx][3*i + j];
                            var a = MarchTables.cornerIndexAFromEdge[edge];
                            var b = MarchTables.cornerIndexBFromEdge[edge]; 
                            var ac = vertices[a];
                            var bc = vertices[b];

                            t[j] = origin + id + ac + (Vector3)(bc - ac) * inter(d[a], d[b]);
                        }
                        triangles.Add(new Triangle{
                            a = t[0],
                            b = t[1],
                            c = t[2]
                        }); 
                    }
                }
            }
        }
        return TriangleMesh.ToMeshData(triangles.ToArray());
    }        
}


