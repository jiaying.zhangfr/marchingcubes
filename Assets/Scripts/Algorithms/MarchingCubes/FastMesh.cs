﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

class FastMesh : Singleton<FastMesh> {
    public ComputeShader shader;
    ComputeBuffer density;
    ComputeBuffer triangles;
    ComputeBuffer count;

    Vector3Int size = ChunkPosition.size + Vector3Int.one;

    new void Awake(){
        base.Awake();
        var len = size.x * size.y * size.z;
        density = new ComputeBuffer(len, sizeof(float));
        triangles = new ComputeBuffer(len * 5, sizeof (float) * 3 * 3, ComputeBufferType.Append);
        count = new ComputeBuffer (1, sizeof(int), ComputeBufferType.Raw);
    }

    /* perform the marching cubes algorithm on the graphics card */
    public MeshData MarchingCubes(float[] dens, Vector3 origin){
        density.SetData(dens);
        shader.SetBuffer(0, "density", density);
        shader.SetInts("size", new int[]{size.x, size.y, size.z});
        shader.SetFloats("origin", new float[]{origin.x, origin.y, origin.z});
        shader.SetBuffer(0, "triangles", triangles);
        
        triangles.SetCounterValue(0);

        var nvoxels = size - Vector3Int.one;
        var ncomputes = new Vector3Int(
            Mathf.CeilToInt((float)nvoxels.x / 7), 
            Mathf.CeilToInt((float)nvoxels.y / 7), 
            Mathf.CeilToInt((float)nvoxels.z / 7)
        );
        
        shader.Dispatch(0, ncomputes.x, ncomputes.y, ncomputes.z);

        ComputeBuffer.CopyCount(triangles, count, 0);
        int[] nArray = {0};
        count.GetData(nArray);
        var n = nArray[0];

        Triangle[] tris = new Triangle[n];
        triangles.GetData(tris, 0, 0, n);
        return TriangleMesh.ToMeshData(tris);
    }        
}


