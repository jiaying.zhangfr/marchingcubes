using UnityEngine;
using System.Linq;

static class SmoothMesh {

    static Vector3[] Normals(MeshData meshdata){
        var normals = new Vector3[meshdata.vertices.Length];
        for(var i=0; i<meshdata.triangles.Length; i+=3){
            var a = meshdata.vertices[meshdata.triangles[i]];
            var b = meshdata.vertices[meshdata.triangles[i + 1]];
            var c = meshdata.vertices[meshdata.triangles[i + 2]];
            var normal = Vector3.Cross(b-a, c-a).normalized;
            normals[meshdata.triangles[i]] += normal;
            normals[meshdata.triangles[i + 1]] += normal;
            normals[meshdata.triangles[i + 2]] += normal;
        }
        for(var i=0; i<normals.Length; i++){
            normals[i].Normalize();
        }
        return normals;
    }

    /* This turns a blocky mesh into a smooth mesh
    In a smooth mesh the vertices are shared between all
    adjacent triangles. In a blocky mesh each triangle has
    its own vertices. We can turn a blocky mesh into a smooth
    mesh by removing all the duplicate vertices and make the 
    triangles reference the now unique corners.*/
    public static Vector3[] SmoothNormals(MeshData mesh){
        var temp = new MeshData();
        /* The vertices in the new mesh are
        all the distinct vertices of the old one */
        temp.vertices = mesh.vertices.Distinct().ToArray();
        /* This dictionary serves as a translator:
        Given a vertex point, what is the position (index)
        inside the new vertices array */
        var d = temp.vertices
            .Select((x,i) => (x,i))
            .ToDictionary(x => x.Item1, x => x.Item2);
        temp.triangles = mesh.triangles
            /* We use the old vertices array 
            as the first translator:
            Given the old index what is the vertex point
            that this triangle corner references to*/
            .Select(x => mesh.vertices[x])
            /* Then we use the above dictionary
            as the second translator:
            Given a vertex point, what is 
            the new position (index)
            inside the new vertices array */
            .Select(x => d[x])
            .ToArray();
        var normals = Normals(temp).ToArray();
        return mesh.vertices
            .Select(x => d[x])
            .Select(x => normals[x])
            .ToArray();   
    }
}

