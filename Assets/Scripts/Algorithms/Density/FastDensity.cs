﻿using UnityEngine;

class FastDensity : Singleton<FastDensity> {
    public ComputeShader shader;
    ComputeBuffer density;
    Vector3Int size = ChunkPosition.size + Vector3Int.one;

    new void Awake(){
        base.Awake();
        var len = size.x * size.y * size.z;
        density = new ComputeBuffer(len, sizeof(float));
    }
    public float[] Density(Vector3 origin){
        shader.SetBuffer(0, "density", density);
        shader.SetInts("size", new int[]{size.x, size.y, size.z});
        shader.SetFloats("origin", new float[]{origin.x, origin.y, origin.z});
        
        var nvoxels = size - Vector3Int.one;
        var ncomputes = new Vector3Int(
            Mathf.CeilToInt((float)nvoxels.x / 7), 
            Mathf.CeilToInt((float)nvoxels.y / 7), 
            Mathf.CeilToInt((float)nvoxels.z / 7)
        );
        
        shader.Dispatch(0, ncomputes.x, ncomputes.y, ncomputes.z);

        var output = new float[density.count];
        density.GetData(output);
        return output;
    }    
    
}
