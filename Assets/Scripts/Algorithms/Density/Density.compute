﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel Density
#include "../../../3rdParty/Compute/Noise.compute"

RWStructuredBuffer<float> density;
uint3 size;
uint3 nThreads;
float3 origin;
static int octaves = 3;
static float weight[] = {10, 5, 2};
static float freq[] = {0.02, 0.04, 0.08};

uint Index(uint3 id){//convension of the 3D index
    return id.x + id.y * size.x + id.z * size.x * size.y;
}


[numthreads(7,7,7)] //343 operations in parallel
void Density (uint3 id : SV_DispatchThreadID)
{
    if(id.x > size.x-1 || id.y > size.y-1 || id.z > size.z-1){
        return;
    }  

    float3 loc = origin + id;
    
    float d = 0;
    for(int i=0; i<octaves; i++)
    {
        float s = snoise(loc.xz * freq[i] + 1000 * (i+1));
        d += s * weight[i] / 2;
    }

    float height = 20;
    for(int i=0; i<octaves; i++)
    {
        float s = snoise(loc.xz * freq[i] + 500 * (i+1));
        height += 2 * s * weight[i] / 2.8;
    }

    for(int i=0; i<octaves; i++)
    {
        float s = snoise(loc * freq[i] + -1000 * (i+1));
        d -= abs(s) * weight[i] / 5;
    }
    
    if(exp(d) < height + d){
        d = exp(d);
    }else{
        d = height + d;
    }

    density[Index(id)] = d - loc.y + 9;
}