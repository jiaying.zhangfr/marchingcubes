﻿using UnityEngine;

static class SlowDensity {
    static Vector3Int size = ChunkPosition.size + Vector3Int.one;
    static int octaves = 3;
    static float[] weight = {10, 5, 2, 1};
    static float[] freq = {0.02f, 0.04f, 0.08f, 0.16f};

    static int Index(Vector3Int id){
        return id.x + id.y * size.x + id.z * size.x * size.y;
    }

    static Vector2 xz(Vector3 v){
        return new Vector2(v.x, v.z);
    }

    static float snoise(Vector2 v){
        return 0.7f * Mathf.PerlinNoise(v.x, v.y);
    }

    static float snoise(Vector3 v){
        return 0.7f * Mathf.PerlinNoise(v.x, v.y) * Mathf.PerlinNoise(v.z, 0.5f);
    }

    static public float[] Density(Vector3 origin){
        var len = size.x * size.y * size.z;
        var density = new float[len];
        for(var x=0; x<size.x; x++){
            for(var y=0; y<size.y; y++){
                for(var z=0; z<size.z; z++){
                    var id = new Vector3Int(x, y, z);
                    var loc = origin + id;
                    var d = 0f;
                    for(var i=0; i<octaves; i++)
                    {
                        var s = snoise(xz(loc) * freq[i] + 1000 * (i+1) * Vector2.one);
                        d += s * weight[i] / 2;
                    }

                    var height = 20f;
                    for(var i=0; i<octaves; i++)
                    {
                        var s = snoise(xz(loc) * freq[i] + 500 * (i+1) * Vector2.one);
                        height += 2 * s * weight[i] / 2.8f;
                    }

                    
                    for(var i=0; i<octaves; i++)
                    {
                        var s = snoise(loc * freq[i] + -1000 * i * Vector3.one);
                        d -= Mathf.Abs(s) * weight[i] / 5;
                    }
                    
                    if(d < 1){
                        d = Mathf.Exp(d);
                    }else if(Mathf.Exp(d) < height + d){
                        d = Mathf.Exp(d);
                    }else{
                        d = height + d;
                    }
                    density[Index(id)] = d - loc.y - 10;
                }
            }
        }
        return density;
    }    
    
}
