﻿using UnityEngine;

class TestBench : Singleton<World> {
    public Material material;
    public RandomObject[] trees;
    Chunk oldChunk;
    void Start(){
        var c = CreateChunk(Vector3Int.zero);
        c.Generate();
        c.MarchingCubes();
        c.Render();
        c.Activate();
        c.PlaceTrees();
        oldChunk = c;
    }

    void Update(){   
        oldChunk.Deactivate();
        var c = CreateChunk(Vector3Int.zero);
        c.Generate();
        c.MarchingCubes();
        c.Render();
        c.Activate();
        c.PlaceTrees();
        oldChunk = c;
    }

    Chunk CreateChunk(Vector3Int chunkPos){
        var worldpos = ChunkPosition.ToWorldPos(chunkPos);
        var chunk = new Chunk(worldpos, transform, material, trees);
        return chunk;
    }

}