using UnityEngine;
using System.Linq;
using System.Collections.Generic;

static class Poisson {

    public static
    Vector2[] Sample(Vector2 size, float radius){

        var grid = new Grid(size, radius);
        List<Vector2> points = new List<Vector2>();
        List<Vector2> spawnPoints = new List<Vector2>();
        spawnPoints.Add(new Vector2(
            Random.Range(0, size.x), 
            Random.Range(0, size.y)
        ));

        while(spawnPoints.Count() > 0){
			var idx = Random.Range(0,spawnPoints.Count);
			var spawn = spawnPoints[idx];
			var accepted = false;

            for (int i = 0; i < 10; i++){
                float angle = Random.value * Mathf.PI * 2;
                Vector2 dir = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
                Vector2 candidate = spawn + dir * Random.Range(radius, 2*radius);
                if (grid.IsValid(candidate)) {
                    points.Add(candidate);
                    spawnPoints.Add(candidate);
                    grid[candidate] = true;
                    accepted = true;
                    break;
                }
            }
            if(!accepted){
                spawnPoints.Remove(spawn);
            }
        }
        return points.ToArray();
    }
}


class SafeArray<T> {
    T[,] arr;

    public SafeArray(int sx, int sy){
        arr = new T[sx, sy];
    }

    public bool isValid(int kx, int ky){
        return 0 <= kx && kx < arr.GetLength(0) && 0 <= ky && ky < arr.GetLength(1);
    }

    T Get(int kx, int ky){
        if(isValid(kx, ky)){
            return arr[kx, ky];
        }else{
            return default(T);
        }
    }

    void Set(int kx, int ky, T v){
        if(isValid(kx, ky)){
            arr[kx, ky] = v;
        }
    }

    public T this[int kx, int ky]{
        get => Get(kx, ky);
        set => Set(kx, ky, value);
    }
}

class Grid {
    SafeArray<bool> grid;
    float cellSize;
    
    public Grid(Vector2 size, float radius){
        cellSize = radius/Mathf.Sqrt(2);
        var n = VectorExt.ToVector2Int(size / cellSize);
        grid = new SafeArray<bool>(n.x, n.y);
    }

    Vector2Int Index(Vector2 key){
        return VectorExt.ToVector2Int(key / cellSize);
    }

    bool[] Neighbours(Vector2 key){
        var idx = Index(key);
        return Functional.Comb(
            Enumerable.Range(-2, 4),
            Enumerable.Range(-2, 4)
        ).Select(v => new Vector2Int(v.Item1, v.Item2))
        .Select(k => idx + k)
        .Select(k => this[k])
        .ToArray();
    }

    public bool IsValid(Vector2 candidate) {
        var idx = Index(candidate);
        return grid.isValid(idx.x, idx.y) && 
            !Neighbours(candidate).Any(x => x);
	}
 
    bool this[Vector2Int key]{
        get => grid[key.x, key.y];
        set => grid[key.x, key.y] = value;
    }

    public bool this[Vector2 key]{
        get => this[Index(key)];
        set => this[Index(key)] = value;
    }
}