﻿using UnityEngine;

class Flying : MonoBehaviour {
    public Transform cam;
    public CharacterController cc;
    readonly float maxSpeed = 10;
    readonly float jumpVelocity = 5;
    readonly float gravity = 9.8f;
    Vector3 velocity = Vector3.zero;

    void Update(){
        
        var mvertical = Input.GetAxis("Mouse Y");
        var mhorizontal = Input.GetAxis("Mouse X");

        var pitch = Input.GetAxis("Vertical");
        var roll = Input.GetAxis("Horizontal");
        var yaw = Input.GetAxis("Horizontal2");
        

        cam.Rotate(-mvertical, mhorizontal, 0);


        var jump = Input.GetAxis("Jump");

        transform.Rotate(pitch, yaw, -roll);
        velocity = transform.forward * maxSpeed;
        cc.Move(velocity * Time.deltaTime);
    }
}