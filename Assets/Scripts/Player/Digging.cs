﻿using UnityEngine;
using System.Collections.Generic;

class Digging : MonoBehaviour {
    public Camera cam;
    public AudioSource audioSource;
    public List<AudioClip> shovels;
    readonly float reach = 5;
    
    AudioClip ShovelSound(){
        return shovels[Random.Range(0, shovels.Count)];
    }

    void Update(){
        if (Input.GetMouseButtonDown(0)){
            var ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, reach)){
                var pos = hit.point + ray.direction * 0.1f;
                var posi = VectorExt.ToVector3Int(pos);
                World.instance.Remove(posi, 3);
                audioSource.PlayOneShot(ShovelSound());
            }
        }
        if (Input.GetMouseButtonDown(1)){
            var ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, reach)){
                var pos = hit.point - ray.direction * 0.1f;
                var posi = VectorExt.ToVector3Int(pos);
                World.instance.Place(posi, 3);
                audioSource.PlayOneShot(ShovelSound());
            }
        }
    }
}