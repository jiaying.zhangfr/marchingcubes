﻿using UnityEngine;
using System;

class Movement : MonoBehaviour {
    public Transform cam;
    public CharacterController character;
    public float walkingVelocity = 5;
    public float jumpVelocity = 5;
    public float gravity = 9.8f;
    Vector3 velocity = Vector3.zero;
    
    public AudioSource audioSource;
    bool is_playing = false;

    // Update is called once per frame
    void Update(){
        var mhorizontal = Input.GetAxis("Mouse X");
        var mvertical = Input.GetAxis("Mouse Y");
        transform.Rotate(Vector3.up * mhorizontal);
        cam.Rotate(Vector3.right * (-mvertical));

        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        var jump = Input.GetAxis("Jump");

        if(character.isGrounded){
            velocity = transform.forward * vertical * walkingVelocity + 
                       transform.right * horizontal * walkingVelocity +
                       transform.up * jump * jumpVelocity;
        }else{
            velocity = velocity - transform.up * gravity * Time.deltaTime;
        }

        RaycastHit hit;
        if((Math.Abs(horizontal) > 0 || Math.Abs(vertical) > 0) && Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down),out hit, 1.4f)){
            if(!is_playing){
                audioSource.timeSamples = UnityEngine.Random.Range(0, audioSource.clip.samples-1);
                audioSource.Play();
                is_playing = true;
            }
        }else{
            if(is_playing){
                audioSource.Stop();
                is_playing = false;
            }
        }
        character.Move(velocity * Time.deltaTime);  //last update time
    }
}
