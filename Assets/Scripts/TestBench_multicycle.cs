﻿using UnityEngine;
using System.Collections.Generic;

class TestBench_multicycle : Singleton<World> {
    public Material material;
    public RandomObject[] trees;
    Chunk oldChunk;
    Queue<Chunk> toMarchQueue = new Queue<Chunk>();  
    Queue<Chunk> toRenderQueue = new Queue<Chunk>();   
    Queue<Chunk> toEmbellishQueue = new Queue<Chunk>(); 
    void Start(){
        var c = CreateChunk(Vector3Int.zero);
        c.Generate();
        c.MarchingCubes();
        c.Render();
        c.Activate();
        c.PlaceTrees();
        oldChunk = c;
    }

    void Update(){   
        if(toEmbellishQueue.Count > 0){
            var c = toEmbellishQueue.Dequeue();
            c.PlaceTrees();
            c.Activate();
            oldChunk.Deactivate();
            oldChunk = c;
        }else if(toRenderQueue.Count > 0){
            var c = toRenderQueue.Dequeue();
            c.Deactivate();
            c.Render();
            toEmbellishQueue.Enqueue(c);
        }else if(toMarchQueue.Count > 0){
            var c = toMarchQueue.Dequeue();
            c.MarchingCubes();
            toRenderQueue.Enqueue(c);
        }else{
            var c = CreateChunk(Vector3Int.zero);
            c.Generate();
            if(!c.Empty()){
                toMarchQueue.Enqueue(c);
            }
        }
    }

    Chunk CreateChunk(Vector3Int chunkPos){
        var worldpos = ChunkPosition.ToWorldPos(chunkPos);
        var chunk = new Chunk(worldpos, transform, material, trees);
        return chunk;
    }

}