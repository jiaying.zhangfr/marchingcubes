﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

class TestBench_multichunk : Singleton<World> {
    public Material material;
    public RandomObject[] trees;
    Chunk oldChunk;

    const int total = 60;

    Vector3Int[] toCreate = Functional.Comb(
        Enumerable.Range(0, total / ChunkPosition.size.x),
        Enumerable.Range(0, total / ChunkPosition.size.y),
        Enumerable.Range(0, total / ChunkPosition.size.z)
    ).Select(x => new Vector3Int(x.Item1, x.Item2, x.Item3))
    .ToArray();

    List<Chunk> toDelete = new List<Chunk>();
    

    void Start(){
        foreach(var x in toCreate)
        {
            var c = CreateChunk(x);
            c.Generate();
            c.MarchingCubes();
            c.Render();
            c.Activate();
            toDelete.Append(c);
        }
    }

    void Update(){ 
        foreach(var c in toDelete)  
        {
            c.Deactivate();
        }
        toDelete = new List<Chunk>();
        foreach(var x in toCreate)
        {
            var c = CreateChunk(x);
            c.Generate();
            c.MarchingCubes();
            c.Render();
            c.Activate();
            toDelete.Append(c);
        }
    }

    Chunk CreateChunk(Vector3Int chunkPos){
        var worldpos = ChunkPosition.ToWorldPos(chunkPos);
        var chunk = new Chunk(worldpos, transform, material, trees);
        return chunk;
    }

}