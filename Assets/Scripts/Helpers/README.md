ProGRESS
===============

Procedural Generation of Realistic Explorable Smooth Surfaces

ProGRESS is a performant framework, based on the Unity engine, for real time rendering of arbitrary voxel density functions, to allow for rapid development of novel approaches in procedural terrain generation.

Requirements
----------------

* Unity 2020.1.4f1

Installation
----------------
* Open Unity Hub and add the root folder of this project
* Open the project in Unity 2020.1.4f1 by double-clicking the project in Unity Hub
* Select the main scene using: File > Open Scene > main.unity
* Build and Run the Project by clicking on the Play Button
* Explore


Usage
----------------
In the hierarchy view there are several elements that can be changed. E.g. the player object can switch from flying to walking by enabling/disable the respective scripts in the inspector. The trees and surface materials can be changed in the inspector of the world object. Inside the ComputeShaders folder there is a ProceduralDensity object which references to a compute shader script in the inspector. This compute shader can be replaced or adapted to change the generated terrain.

Flight Controls
----------------
* WS: pitch
* AD: yaw
* QE: roll
* mouseL: dig
* mouseR: place

Movement Controls
-----------------
* WS: forward/backward
* AD: left/right
* space: jump
* mouseL: dig
* mouseR: place
