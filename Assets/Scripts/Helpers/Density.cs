using UnityEngine;
using System.Linq;

class Density {
    /* This is the 3 dimensional density function
    used for the Marching Cubes algorithm.
    Each position in the 3D space can define a density.
    When the density is sufficiently large it is inside,
    When it is too small it is outside.
    However to transfer it to the graphics card we have
    to flatten it into a 1D array. Here this is done
    by directly writing to the 1D array and overloading
    the [] operator to make it look like we can access 
    it with a Vector3 */
    public float[] densities;
    public Vector3Int size;
    
    public Density(Vector3Int size){
        this.size = size;
        densities = new float[size.x * size.y * size.z];
    }

    public Density(Vector3Int size, float[] data){
        this.size = size;
        densities = data;
    }

    public float this[Vector3Int key]{
        get => densities[Index(key)];
        set => densities[Index(key)] = value;
    }

    /* test whether the density function of
    the chunk contains the block */
    public bool Contains(Vector3Int key){
        return 0 <= key.x && key.x < size.x &&
            0 <= key.y && key.y < size.y && 
            0 <= key.z && key.z < size.z;
    }

    public bool Empty(){
        return densities.All(x => x > 0) || 
            densities.All(x => x <= 0);
    }

    int Index(Vector3Int key){
        return key.x + size.x * key.y + size.x * size.y * key.z;
    }
}
