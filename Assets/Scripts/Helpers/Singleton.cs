using UnityEngine;

class Singleton<T> : MonoBehaviour where T : Singleton<T> {
    public static T instance;
    protected void Awake(){
        instance = this as T;
    }
}