﻿using UnityEngine;

/* Data structure similar to Unity Mesh,
but without all the implicit computations */
public struct MeshData {
    public Vector3[] vertices; //Position
    public int[] triangles; //Index
    public Vector2[] uv;
    public Vector3[] normals;

    public Mesh toMesh(){
        var mesh = new Mesh {
            vertices = vertices,
            triangles = triangles,
            uv = uv,
            normals = normals
        };
        mesh.Optimize();
        return mesh;
    }

}
