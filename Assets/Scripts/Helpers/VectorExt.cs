using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

static class VectorExt {

    public static IEnumerable<Vector3Int> Sphere(int radius){
        return Range(-radius * Vector3Int.one, radius * Vector3Int.one)
            .Where(x => x.magnitude < radius);
    }


    public static IEnumerable<Vector3Int> Range(Vector3Int start, Vector3Int end){
        var size = end - start + Vector3Int.one;
        return Functional.Comb(
            Enumerable.Range(start.x, size.x),
            Enumerable.Range(start.y, size.y),
            Enumerable.Range(start.z, size.z)
        ).Select(x => new Vector3Int(x.Item1, x.Item2, x.Item3));
    }

    public static IEnumerable<Vector3Int> Range(Vector3Int end){
        return Range(Vector3Int.zero, end);
    }

    public static IEnumerable<Vector2Int> Range(Vector2Int start, Vector2Int end){
        var size = end - start + Vector2Int.one;
        return Functional.Comb(
            Enumerable.Range(start.x, size.x),
            Enumerable.Range(start.y, size.y)
        ).Select(x => new Vector2Int(x.Item1, x.Item2));
    }

    public static IEnumerable<Vector2Int> Range(Vector2Int end){
        return Range(Vector2Int.zero, end);
    }

    public static Vector2Int ToVector2Int(Vector2 v){
        return new Vector2Int(
            (int)Math.Floor(v.x),
            (int)Math.Floor(v.y)
        );
    }

    public static Vector3Int ToVector3Int(Vector3 v){
        return new Vector3Int(
            (int)Math.Floor(v.x),
            (int)Math.Floor(v.y),
            (int)Math.Floor(v.z)
        );
    }

    public static Vector3 DivideBy(this Vector3 a, Vector3 b){
        return new Vector3(
            a.x / b.x,
            a.y / b.y,
            a.z / b.z
        );
    }
}