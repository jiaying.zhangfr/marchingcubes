using System.Collections.Generic;

static class Functional{

    public static
    IEnumerable<(T1, T2)> Comb<T1, T2>(
        IEnumerable<T1> a,
        IEnumerable<T2> b
    ){
        foreach(var x in a){
            foreach(var y in b){
                yield return (x, y);
            }
        }
    }

    public static
    IEnumerable<(T1, T2, T3)> Comb<T1, T2, T3>(
        IEnumerable<T1> a,
        IEnumerable<T2> b,
        IEnumerable<T3> c
    ){
        foreach(var x in a){
            foreach(var y in b){
                foreach(var z in c){
                    yield return (x, y, z);
                }
            }
        }
    }
}

