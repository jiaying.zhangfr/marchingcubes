using UnityEngine;

static class ChunkPosition {

    public static readonly Vector3Int size = new Vector3Int(20, 20, 20);

    /* Convert world-coordinates to chunk-coordinates
    a chunk has a size, e.g. 20x20x20. Therefore the worldpos
    (25,45,5) is actually inside the chunk (1,2,0) */
    public static Vector3Int ToChunkPos(Vector3 worldPos){
        var res = worldPos.DivideBy(size);
        return VectorExt.ToVector3Int(res);
    }

    /* convert chunk-coordinates back into world-coordinates
    The chunk (3,4,5) is located at (60,80,100) in 
    world coordinates */
    public static Vector3 ToWorldPos(Vector3Int chunkPos){
        return chunkPos * size;
    }
}