﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

class World : Singleton<World> {
    public Material material;
    public Transform player;
    public RandomObject[] trees;
    readonly Vector3Int viewDistance = new Vector3Int(10, 2, 10);

    readonly ConcurrentDictionary<Vector3Int, Chunk> dictionary = new ConcurrentDictionary<Vector3Int, Chunk>();
    Vector3Int oldPosition;

    Queue<Vector3Int> toCreateQueue = new Queue<Vector3Int>();
    Queue<Chunk> toMarchQueue = new Queue<Chunk>();  
    Queue<Chunk> toRenderQueue = new Queue<Chunk>();   
    Queue<Chunk> toEmbellishQueue = new Queue<Chunk>(); 

    // Start is called before the first frame update
    void Start(){   
        var pos = ChunkPosition.ToChunkPos(player.position);//player current chunk position
        var chunksInView = ChunksInView(pos);
        var toCreate = chunksInView.Except(dictionary.Keys).ToArray(); //
        foreach(var chunkPos in toCreate){
            var c = CreateChunk(chunkPos);
            c.Generate();
            c.MarchingCubes();
            c.Render();
            c.Activate();
            c.PlaceTrees();
        }
        oldPosition = pos;       
    }

    // Update is called once per frame
    void Update(){   
        var pos = ChunkPosition.ToChunkPos(player.position);//player current chunk position
        if(pos != oldPosition){
            var inView = ChunksInView(pos);
            dictionary.Keys
                .Except(inView)
                .Select(x => dictionary[x])
                .ForEach(x => x.Deactivate());
            dictionary.Keys
                .Intersect(inView)
                .Select(x => dictionary[x])
                .ForEach(x => x.Activate());
            toCreateQueue.Clear();
            var toCreate = inView.Except(dictionary.Keys)
                .OrderBy(x => (pos - x).magnitude);
            toCreateQueue.Enqueue(toCreate);
            oldPosition = pos;
        }  
        if(toEmbellishQueue.Count > 0){
            var c = toEmbellishQueue.Dequeue();
            c.PlaceTrees();
        }else if(toRenderQueue.Count > 0){
            var c = toRenderQueue.Dequeue();
            c.Render();
            c.Activate();
            toEmbellishQueue.Enqueue(c);
        }else if(toMarchQueue.Count > 0){
            var c = toMarchQueue.Dequeue();
            c.MarchingCubes();
            toRenderQueue.Enqueue(c);
        }else if(toCreateQueue.Count() > 0){
            var p = toCreateQueue.Dequeue();
            var c = CreateChunk(p);
            c.Generate();
            if(!c.Empty()){
                toMarchQueue.Enqueue(c);
            }
        }
    }

    Chunk CreateChunk(Vector3Int chunkPos){
        var worldpos = ChunkPosition.ToWorldPos(chunkPos);
        var chunk = new Chunk(worldpos, transform, material, trees);
        dictionary[chunkPos] = chunk;
        return chunk;
    }

    /* Chunk-coordinates of all Chunks that are
    within viewdistance of the player */
    IEnumerable<Vector3Int> ChunksInView(Vector3Int pos){
        return VectorExt.Range(-viewDistance, viewDistance)
        .Select(x => x + pos);
    }

    IEnumerable<Vector3Int> ChunksCloseBy(Vector3Int pos){
        var v = new Vector3Int(1,1,1);
        return VectorExt.Range(-v, v)
            .Select(x => x + pos);
    }

    void ForeachInRadius(Vector3 pos, int radius, Action<Chunk, Vector3, float> fnc){
        var chunkpos = ChunkPosition.ToChunkPos(pos);
        var cubes = VectorExt.Sphere(radius)
            .Select(x => x + pos);
        var chunks = ChunksCloseBy(chunkpos)
            .Select(x => dictionary[x])
            .Where(x => cubes.Any(y => x.Contains(y)));
        chunks.ForEach(x =>
            cubes.Where(y => x.Contains(y))
                .ForEach(y => fnc(x, y, radius - (pos-y).magnitude))
        );
        chunks.ForEach(x => x.MarchingCubes());  
        chunks.ForEach(x => x.Render());    
    }

    /* Remove the Block at pos in world-coordinates,
    as we now use a density function we actually have
    to talk in means of corners. The same corner can
    appear in mutliple chunks. We have to make sure
    that the different copies always have the same
    value.*/
    public void Remove(Vector3 pos, int radius){
        ForeachInRadius(pos, radius, (x,y,z) => x.Remove(y,z));
    }

    /* Add a Block at pos in world-coordinates 
    as we now use a density function we actually have
    to talk in means of corners. The same corner can
    appear in mutliple chunks. We have to make sure
    that the different copies always have the same
    value.*/
    public void Place(Vector3 pos, int radius){
        ForeachInRadius(pos, radius, (x,y,z) => x.Place(y,z));
    }
}