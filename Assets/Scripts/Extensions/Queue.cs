using System.Collections.Generic;

static class QueueExtension {
    public static void Enqueue<T>(this Queue<T> queue, IEnumerable<T> elements){
        elements.ForEach(x => queue.Enqueue(x));
    }
}