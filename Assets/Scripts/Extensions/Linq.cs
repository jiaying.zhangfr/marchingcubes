using System;
using System.Collections.Generic;
using System.Linq;

static class LINQExtension {
    public static void ForEach<T>(this IEnumerable<T> list, Action<T> fnc){
        list.ToList().ForEach(fnc);
    }
}