using UnityEngine;

public class RandomObject : MonoBehaviour {
    public GameObject[] objs;
    public Vector3 rotation;
    public float scale;
    public float distance;
}