﻿Shader "Custom/Terrain"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _SecondTex ("Base (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows
        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        #include "../../3rdParty/Compute/Noise.compute"

        #define M_PI 3.1415926535897932384626433832795

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 worldNormal;
        };

        half _Glossiness;
        half _Metallic;
        sampler2D _MainTex;
        sampler2D _SecondTex;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf(Input IN, inout SurfaceOutputStandard o) 
        {
            float h = 0.5 + IN.worldNormal.y * 0.5;
            h = pow(h * 1.1, 5);
            h = h - 0.1 * snoise(IN.worldPos + 1000);
            h = max(min(h, 1), 0);


            float rot = snoise(IN.worldPos- 1000);
            float angle = rot > 0 ? M_PI : 0;
            float2x2 rotation = {cos(angle), sin(angle), -sin(angle), cos(angle)};
            float2 uv = mul(rotation, IN.uv_MainTex);

            /* [-1, 1] based on angle*/
            //float h = max(IN.worldNormal.y, 0);
            /* interpolate between the two colors based on the difference between the vectors. */
            float3 grass = tex2D(_MainTex, uv).rgb;
            float3 stone = tex2D(_SecondTex, uv).rgb;
            float3 color = lerp(stone, grass, h);
            o.Albedo = color.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
        }
        ENDCG
    } 
    Fallback "Diffuse"
}